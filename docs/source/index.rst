########################
Automated SEPA Checklist
########################

Welcome to the documentation for the Automated SEPA Checklist. Use the table of contents to jump to a section in the documentation or click ahead to the Introduction using the 'Next' button.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   introduction.rst
   roles.rst
   sepa_checklist.rst


.. automodule:: questions

.. automodule:: util

.. automodule:: main

   












