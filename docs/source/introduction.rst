Introduction
============

Under the State Environmental Policy Act (SEPA), local governments and 
state agencies use a checklist (hereafter, the SEPA checklist) to help 
them determine whether a project proposal is likely to have significant 
impacts to the environment (source: Ecology_). The Washington Department 
of Natural Resources (DNR) uses a modified SEPA Checklist with 
agency-specific questions for determining likely impacts from its timber 
sales. The agency fills out approximately 200 of these forms every year;
thus they represent a significant expenditure of staff time. 

In 2018 the SEPA Center at DNR revised the checklist language over which
the agency exercises control. This goal of this project was to refine 
vague or confusing passages, and improve the alignment between checklist 
questions and agency priorities. Near the end of this process we began a
separate project to auto-fill the most data-intensive or boilerplate 
sections of the checklist, known as the automated SEPA Checklist. 

The automated SEPA Checklist is a small collection of Python scripts 
that generate a partially-completed checklist on demand, with the intended 
purpose to:


**1. Reduce the overhead associated with submitting boilerplate answers**	
	
Many of the questions on the standard, Ecology-based SEPA Checklist 
simply do not apply to 	timber 	sales. Our scripts-based process 
automatically fills in these questions with answers that 	
have been agreed upon in advance by staff in the Division and regions. 

**2. Ease the burden of submitting data-heavy answers**

Prior to the establishment of this project it was common for 
staff in different offices to copy-paste content from an ad hoc 
assemblage of other reports and databases. This was a rational 	approach 
to the onerous responsibility of knowing where to find appropriate data for 
questions ranging in content from soil types to the presence of rare
plants. However, this approach was also time-intensive and inefficient.
The automated checklist fetches data from the appropriate source to 
generate completed tables and check-boxes.  

**3. Improve consistency by enforcing standard answers**

In addition to the time wasted by inefficient data retrieval methods, 
copy-pasted text and data from various sources introduced significant 
inconsistencies in how questions were answered across offices and regions. 
The automated checklist encourages consistent answers by suggesting skeleton text 
and data that can be augmented as needed by on-the-ground expertise. 

.. _Ecology: https://ecology.wa.gov/regulations-permits/sepa/environmental-review/sepa-guidance/sepa-checklist-guidance 