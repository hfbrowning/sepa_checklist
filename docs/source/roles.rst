Roles and Responsibilities
==========================

- **Script Developer:** Hilary Browning  
- **Script Maintainer(s):** Hilary Browning; *Forest Resources Division, GIS Section* 
- **Host Site Maintainer:** Kristin Jamison; *Information Technology Division* 
- **SEPA Language Stewards:** Patrick Ferguson; *Product Sales and Leasing Division* & Sean Goldsmith; *SEPA Center*
- **Audience:** State lands foresters filling out a SEPA Checklist


How it works
============

When the ``main.py`` script is run, it draws from corporate data sets to calculate answers to various SEPA Checklist questions. For the most part one checklist question corresponds to one function in the ``questions.py`` script, which is imported by ``main.py``. Each question functions returns a dictionary answer, which is then collated with all other answers at the end to return a master dictionary. This is passed to the 3rd party package ``doctxpl`` renderer, which uses a .docx file as a template. The syntax of ``doctxpl`` is modeled upon the popular template engine ``jinja2``. The resulting output .docx file is written to the agency TEMP folder and additionally emailed to the user's email address. 

The main script can be launched either from the command line by passing in parameters for the administrative unit name, the sale name, and a user's email; or alternatively by launching from the http://management/SepaMaps/ browser. This latter delivery method uses AppWorx (ITD) on the back end to pass inputs to the script. 


Making changes to the scripts
=============================

The scripts to drive this automated process are housed at:

``\\dnr\divisions\FR_DATA\forest_info_2\gis\projects\sepa_checklist``

These scripts may be modified as needed and re-run to test changes. Please note that outputs are designed to write to the agency TEMP folder at: ``//dnr/regions/TEMP``. This folder is deleted nightly. All changes made locally within the project folder must be passed along to Kristin Jamison to be promoted to production. 


