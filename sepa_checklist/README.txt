A readme to describe the contents of 'sepa_checklist'
Hilary Browning
9:29 AM 9/14/2018

This folder contains all of the scripts for running the automated SEPA Checklist. The folder also is named sepa_checklist only due to technical reasons involved with Sphinx documentation. It could more properly be considered the 'script' folder.

top-level folder
----------------

- __init__.py::  an empty script that allows the folder to be treated as a package
- arcpy_logging.py:: contains a class to convert arcpy GetMessages() into logging module messages
- data.py:: contains a couple of pieces of data and no programming logic
- main.py:: the main script for running the process
- making_batch_csv.py:: the script for running the process in batch mode
- questions.py:: the script that contains all of the questions 
- util.py:: utility helper functions. Contains no arcpy calls, which allows it to be tested more easily by unit testing (in the tests folder)

misc
----
All of the scripts in here are either half-started efforts at new work, or are archive functions that were once needed for old questions on the checklist. 


logs
----
This folder is where logs are written to during a run. 
