"""Contact information for region offices."""
REGION_CONTACTS = {'NORTHEAST':    ("Northeast Region \n\t"
                                    "225 S Silke Road \n\t"
                                    "Colville, WA 99114 \n\t"
                                    "(509) 684-7474"),

                   'NORTHWEST':    ("Northwest Region \n\t"
                                    "919 N Township Street \n\t"
                                    "Sedro Woolley, WA 98284 \n\t"
                                    "(360) 856-3500"),

                   'OLYMPIC':      ("Olympic Region \n\t"
                                    "411 Tillicum Lane \n\t"
                                    "Forks, WA 98331 \n\t"
                                    "(360) 374-2800"),

                   'PACIFIC CASC': ("Pacific Cascade Region \n\t"
                                    "601 Bond Road \n\t"
                                    "PO Box 280 \n\t"
                                    "Castle Rock, WA 98611 \n\t"
                                    "(360) 577-2025"),

                   'SO PUGET':     ("South Puget Sound Region \n\t"
                                    "950 Farman Avenue N \n\t"
                                    "Enumclaw, WA 98022 \n\t"
                                    "(360) 825-1631"),

                   'SOUTHEAST':    ("Southeast Region \n\t"
                                    "713 Bowers Road \n\t"
                                    "Ellensburg, WA 98926  \n\t"
                                    "(509) 925-8510")
                   }

UNEVEN_CODES = ['COMMRCL_THIN', 'LAT_RTN_THIN', 'SELECT_PROD', 'UNEVNAGE_MGT', 'VARIABL_THIN', 'PATCH_REGEN',
                'TWO_AGE_MGT']

EVEN_CODES = ['CLEAR_CUT', 'SHELTER_INT', 'SEEDTREE_INT', 'SEEDTREE_REM', 'TEMP_RET_1ST', 'PILE',
              'TEMP_RET_REM', 'VRH', 'SHELTER_REM', 'LAND_USE_CONV', 'GATHER']