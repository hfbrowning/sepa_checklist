# =================================================================
# script name: main.py
#
# Description: the main script for running the automated SEPA Checklist
#
# Called by: APPWORX job: SEPA_CHECKLIST
# Inputs: user choices are passed to the appworx job through the SEPA
# Checklist web page.
#
# Outputs: processing directory at \\dnr\regions\temp\sepa_checklist
#          fileGDB, .docx
#
# Author: Hilary Browning, October, 2018
#         DNR Forest Resources Div., Forest Informatics
#
# Modifications:
# 10/2018 K. Jamison - modifications for Appworx batch processing
#
# =================================================================

import logging
import os
import sys
import time
import re

import arcpy

import questions
import util
from arcpy_logging import ArcpyLog

EMAIL_SENDER = 'orapipe@dnr.wa.gov' # use for Appworx
ADMIN_EMAIL = ['Hilary.Browning@dnr.wa.gov', 'Kristin.Jamison@dnr.wa.gov']
# ADMIN_EMAIL = ['Kristin.Jamison@dnr.wa.gov'] # kj testing
mail_server = os.getenv("MAILRELAY")


class SetUp(object):
    """
    Set up temp folders, log files, and global variables.

    The folder tree for this set up looks as follows (using attached
    attribute names rather than paths):

    .. code-block:: text

        |-- workspace
            |-- folder_name (all up to this point = work_folder)
                |-- proc_id (^= process_path)
                    |-- gdb_name.gdb (^= gdb_full_path)


    Using ``^=`` as short-hand for 'all up to this point, ``os.path.join()``'.

    Attributes
    ----------
    folder_name: str
        Hard-coded to 'SEPA_CHECKLIST'
    ropa_path: str
        Hard-coded ROPA base path
    workspace: str
        Hard-coded DNR TEMP folder
    work_folder: str
        Joined workspace and folder_name

    data_home: str
        System env variable 'DATAHOME'
    mail_server: str
        System env variable 'MAILRELAY'
    myenv: str
        System env variable 'MYENV'
    proc_drive: str
        System env variable 'PROCDRIVE'
    ropa_instance: str
        System env variable 'ROPA_SDE_INSTANCE'
    connect_home: str
        SDE connection home
    log_home: str
        Folder where logs written

    date_time_stamp: str
        Date & time string-formatted to: '%m-%d-%H%M'
    log: logging object
        Module level logging instance
    process_path: str
        Full path to the folder where processing occurs
    gdb_full_path: str
        Full path to geodatabase where outputs saved
    """

    def __init__(self, log_level, proc_id, email, gdb_name):
        """
        Parameters
        ----------
        log_level: str
            Logging severity conformant to standard logging
        proc_id: str
            Process ID from batch processer
        gdb_name: str
            Geodatabase name to create/write to
        """
        print "Setting system variables"

        # Hard-coded
        self.folder_name = "SEPA_CHECKLIST"
        # self.ropa_path = '\\\\dnr\\divisions\\FR_DATA\\forest_info_2\\gis\\tools\\sde_connections_read\\ropa_gis_layer_user_direct.sde'
        self.workspace = "\\\\DNR\\REGIONS\\TEMP"
        self.work_folder = os.path.join(self.workspace, self.folder_name)

        # System Vars
        # self.data_home = 'data_home'  # os.getenv("DATAHOME")
        # self.mail_server = 'mailrelay'  # os.getenv("MAILRELAY")
        # self.myenv = 'myenv'  # os.getenv("MYENV")
        # self.proc_drive = 'proc_drive'  # os.getenv("PROCDRIVE")
        # self.ropa_instance = 'ropa_sde_instance'  # os.getenv("ROPA_SDE_INSTANCE")
        # self.connect_home = os.path.join(self.proc_drive, "sde_connections")
        # self.log_home = 'logs' #os.path.join(self.data_home, "manage\\logs")

        self.connect_home = os.getenv("CONNECTHOME")   # Appworx batch
        self.ropa_instance = os.getenv("ROPA_SDE_INSTANCE")   # Appworx batch
        self.ropa_path = os.path.join(self.connect_home, self.ropa_instance + "_sepamaps_user.sde")   # Appworx batch
        self.data_home = os.getenv("DATAHOME")   # Appworx batch
        self.log_home = os.path.join(self.data_home, "manage\\logs")   # Appworx batch

        # Inputs
        self.date_time_stamp = time.strftime('%m%d%Y_%H%M')
        self.gdb_name = gdb_name
        self.log_level = log_level
        self.recipient_id = self.parse_email_address(email_address=email)
        self.process_id = "{}_{}".format(self.recipient_id, proc_id)

        # Run set up methods
        self.log, self.log_path = self.create_logger(self.log_level)
        self.check_work_folder_exists()
        self.process_path = self.create_process_folder()
        self.gdb_full_path = self.create_gdb()

    def create_logger(self, log_level):
        """Create a logging instance at log home with date stamp.

        This logger is the parent of what is being passed around inside the rest of
        the module (including what is passed into, and then out of, ArcpyLog) and it is
        what is passed to getlogger in the sub-modules.

        Parameters
        ----------
        log_level: str
            Logging severity conformant to standard logging

        Returns
        -------
        logging object
            Module level logging instance
        log_path str
            Path to log file on disk

        """
        # kj - file_name = "log_{}.log".format(self.date_time_stamp)
        file_name = "sepa_checklist_{}.log".format(self.date_time_stamp)
        log_path = os.path.join(self.log_home, file_name)
        level = log_level.upper()
        switcher = {"DEBUG": logging.DEBUG,
                    "INFO": logging.INFO,
                    "WARNING": logging.WARNING,
                    "ERROR": logging.ERROR,
                    "CRITICAL": logging.CRITICAL}
        logging.basicConfig(filename=log_path,
                            format="%(asctime)s | %(name)s [Line %(lineno)d] | %(levelname)s: %(message)s",
                            datefmt="%m/%d/%Y %H:%M:%S",
                            level=switcher[level])
        logger = logging.getLogger("root")
        return logger, os.path.abspath(log_path)

    def check_work_folder_exists(self):
        """Creates folder at :code:`//DNR/REGIONS/TEMP/SEPA_CHECKLIST` if it does not exist.

        Returns
        -------
        None
        """
        os.chdir(self.workspace)
        try:
            os.mkdir(self.folder_name)
            self.log.info("Made new work folder at: {}".format(self.work_folder))
            # Note: The syntax of 0o777 is for Python 2.6 and 3+.
            os.chmod(self.folder_name, 0o777)
        except WindowsError:
            self.log.info("Work folder exists at: {}".format(self.work_folder))

    def create_process_folder(self):
        """Creates folder for processing if it does not exist.

        Returns
        -------
        str
            Full path to the folder where processing occurs
        """
        os.chdir(self.work_folder)
        full_path = os.path.join(self.work_folder, self.process_id)
        if not os.path.exists(self.process_id):
            os.mkdir(self.process_id)
            self.log.info("Made new processing folder at: {}".format(full_path))
            # Note: The syntax of 0o777 is for Python 2.6 and 3+.
            os.chmod(self.process_id, 0o777)
        else:
            self.log.info("Pre-existing processing folder found at: {}".format(full_path))
        return full_path

    def create_gdb(self):
        """Creates geodatabase for processing if it does not exist.

        Returns
        -------
        str
            Full path to geodatabase where outputs will be saved

        """
        arcpy.env.workspace = self.work_folder
        checklist_gdb = os.path.join(self.process_path, self.gdb_name) + ".gdb"
        if arcpy.Exists(checklist_gdb):
            self.log.info("Pre-existing gdb folder found at: {}".format(checklist_gdb))
            return checklist_gdb
        else:
            arcpy.CreateFileGDB_management(self.process_path, self.gdb_name, "CURRENT")
            self.log.info("Created Sepa Checklist GDB at: {}".format(checklist_gdb))
            return checklist_gdb

    def parse_email_address(self, email_address):
        """
        Take in an email address and return a usable string ID.

        This function takes in the user-submitted email and address
        and attempts to turn it into an identifier that can be used
        in the process_id folder name.

        Notes
        -----
        The function assumes that the last instance of an '@' is the
        domain and that everything following it (like gmail, or dnr.wa.gov)
        should be dropped. Subsequently all other punctuation is
        dropped. If the email address is very poorly formed (no '@') the
        process folder will just use the AppWorx process ID like before -
        but of course the user will also not be emailed a finished
        checklist :)

        Parameters
        ----------
        email_address: str
            User-input email address

        Returns
        -------
        str

        """
        email = ''.join(email_address.split('@')[:-1])
        return re.sub('[\W_]+', '', email)


def arcpy_defaults(log=None):
    """Set standard arcpy environmental settings.

    This function sets all of the arcpy settings that are
    used most commonly in FRD. The optional logging parameter
    allows sending a message to the log file.

    Parameters
    ----------
    log: logging object, optional
        module-level logger

    Returns
    -------
    None
        Changes environmental settings (side effect)
    """
    arcpy.env.overwriteOutput = True
    arcpy.SetLogHistory(False)
    arcpy.env.pyramid = "None"
    arcpy.env.rasterStatistics = "STATISTICS"
    arcpy.env.XYResolution = "0.0005 METERS"
    arcpy.env.XYTolerance = "0.001 METERS"
    arcpy.env.outputCoordinateSystem = 2927  # WASPSNAD83HARNFEET
    if log:
        log.info("Arcpy defaults set")


def read_in_clip(in_fc, out_fc, ropa_path, where_clause="", intersect_lyr=None, clip_fc=None):
    """Read in ROPA data and constrain it by a clip, query, and/or intersection.

    Make a clipped ArcGIS feature class from ROPA based upon:

        1. an optional SQL query
        2. optional clip by layer, and
        3. optional select by location from spatial extent of another layer

    Defaults will simply copy in all features.

    Parameters
    ----------
    in_fc: str
        Feature class name in corporate (ROPA/SHARED_LM/SHARED_LRM).
        Example: :code:`'ROPA.ROADS'`
    out_fc: str
        Full path name where output should be saved
    ropa_path: str
        ROPA base path
    where_clause: str, optional
        SQL query
    intersect_lyr: str, optional
        Feature layer with which to intersect the input
    clip_fc: str, optional
        Feature class with which to clip the input

    Returns
    -------
    None
        Writes to disk (side effect)

    """
    in_fc = os.path.join(ropa_path, in_fc)
    arcpy.MakeFeatureLayer_management(in_fc, 'fl', where_clause)
    if intersect_lyr:
        arcpy.SelectLayerByLocation_management('fl', 'INTERSECT', intersect_lyr)

    if clip_fc:
        arcpy.Clip_analysis('fl', clip_fc, out_fc)
    else:
        arcpy.CopyFeatures_management('fl', out_fc)


def run_analyses(fc_to_run, ts_layer, gdb_path, top_n=5):
    """Run all SEPA Checklist question functions.

    This is essentially a wrapper function that passes inputs to
    individual questions on the SEPA checklist. The latter two
    parameters pass directly to questions and thus have little
    inherent meaning here.

    Parameters
    ----------
    fc_to_run: str
        Path to FC on disk corresponding to Special Concerns data
    ts_layer: str
        Feature layer corresponding to timber sale information
    gdb_path: str
        Path on disk where intermediary outputs will be written
    top_n: int
        Number of entries in SEPA checklist tables

    Returns
    -------
    dict
        Dictionary of all SEPA answers
    """
    context = questions.simple_info(ts_layer, gdb_path)

    # This is more readable if we simply go through each one
    water_problems = questions.find_303d_problems(fc_to_run)
    species = questions.find_endangered_animals(ts_layer)
    plants = questions.find_endangered_plants(ts_layer)
    soils = questions.describe_soils(fc_to_run, top_n)
    road_miles = questions.find_road_miles(fc_to_run)
    fp_apps = questions.get_fp_applications(fc_to_run, gdb_path)
    slope = questions.find_slope(fc_to_run, gdb_path)
    wau_desc = questions.get_wau_description(fc_to_run, gdb_path)

    for analysis in [water_problems, species, plants, soils, road_miles,
                     fp_apps, slope, wau_desc]:
        context.update(analysis)

    return context


def failure_mailer(recipient, process_id, log_path, ts_name):
    """
    If the process fails, email both administrators and user.

    Parameters
    ----------
    recipient: str
        User's email address
    process_id: str
        ID of processing folder
    log_path: str
        Path to logfile
    ts_name: str
        Name of timber sale run

    Returns
    -------
    None
        Sends email (side effect)

    """
    date_stamp = time.strftime('%m/%d/%Y')
    # Email user
    user_header = "Failure: Your SEPA Checklist failed to run"
    user_text = ("Your request on {} to the Automated SEPA Checklist process "
                 "for timber sale '{}' has failed to process correctly. "
                 "Administrators have been "
                 "alerted to the problem and will contact you shortly. "
                 "If you are not contacted within 1 business day, please "
                 "feel free to email us at SEPACenter@dnr.wa.gov "
                 "to follow up. Please do not attempt to create this "
                 "particular SEPA Checklist again until the issue has been "
                 "resolved.").format(date_stamp, ts_name)
    util.send_mail(mail_server, EMAIL_SENDER, [recipient], user_header, user_text)

    # Email admins
    admin_header = "Failure: An request to the SEPA Checklist process failed."
    admin_text = ("A request to run the automated SEPA checklist has failed. "
                  "This request had process_id: {} and was initiated by user {}."
                  " Please see the attached log file for further details.".format(process_id,
                                                                                  recipient))
    file_to_attach = log_path
    util.send_mail(mail_server, EMAIL_SENDER, ADMIN_EMAIL, admin_header, admin_text, [file_to_attach])


def success_mailer(recipient, docx_attachment):
    """
    If the process runs to completion, send the user an email with results.

    Do not send anything to the administrators upon success.

    Parameters
    ----------
    recipient: str
        User's email address
    docx_attachment: str
        Path to completed docx

    Returns
    -------
    None
        Sends email (side effect)

    """
    header = "Your SEPA Checklist is Ready"
    text = ("The automated SEPA checklist process ran correctly. "
            "Your results are attached.")
    file_to_attach = docx_attachment
    util.send_mail(mail_server, EMAIL_SENDER, [recipient], header, text, [file_to_attach])


def main(args):

    try:
        process_id = args[0]  # 'TESTING'
        recipient = args[1]  # 'Hilary.Browning@dnr.wa.gov'
        admin_name = args[2]  # 'DELPHI'
        ts_name = args[3]  # 'COHO'
        contract_num = args[4]  # 64165
        # debugging = args[5]  # 'False'
        debugging = 'False'
        # base_path = args[6]  # 'C:/hbro490'
        base_path = os.getenv("WINEXECHOME")  # z:\batch\devl(or demo, prod)
        folder_ts_admin_name = "{0}_{1}".format(util.replace_bad_chars(ts_name),
                                                util.replace_bad_chars(admin_name))

        # Set up folders and loggers
        mem = SetUp('DEBUG', process_id, recipient, folder_ts_admin_name)
        log = mem.log

        # Check out standard arcpy things
        arcpy_defaults(log=log)

        # After creating this 'arcpy logger' you can run alog.log() to
        # get any arcpy messages you want
        alog = ArcpyLog(log, mem.log_level)

        # Data sources
        TIMBER_SALES = os.path.join(mem.ropa_path, 'SHARED_LRM.TS_FMA_SV')

        layer = 'ts_layer'
        wc = "TS_NM = '{0}' AND TS_ID = {1}".format(ts_name, contract_num)
        arcpy.MakeFeatureLayer_management(TIMBER_SALES, layer, where_clause=wc)

        # This is what we want to do (below) BECAUSE it doesn't cut things into little
        # pieces. The layers stay overlapping, which is actually desirable because it's
        # more efficient and it's what we actually want (because the questions apply that way)
        temp_fc = os.path.join(mem.gdb_full_path, 'clipped_special_concerns')
        read_in_clip(in_fc="ROPA.LRM_SPECIAL_CONCERNS",
                     out_fc=temp_fc,
                     ropa_path=mem.ropa_path,
                     intersect_lyr=layer,
                     clip_fc=layer)

        # Actually run the analyses..
        print("Running analyses...")
        context = run_analyses(temp_fc, layer, mem.gdb_full_path, top_n=5)
        print("Process #{} finished successfully".format(process_id))

        # If debugging we only want the csv of data. If not, we want docx files.
        if debugging == 'True':
            return context
        else:
            # Write out to template
            template = os.path.join(base_path, 'sepa_checklist/templates/template.docx')
            output_docx = os.path.join(mem.process_path,
                                       "{}_autofilled_{}.docx".format(ts_name, mem.date_time_stamp))
            util.docx_render(template, context, output_docx)

            # And email the final product
            success_mailer(recipient=recipient, docx_attachment=output_docx)

    except Exception as e:
        logging.exception("Fatal error in main program: ")
        failure_mailer(recipient=recipient, process_id=process_id, log_path=mem.log_path,
                       ts_name=ts_name)
        sys.exit("\n{}\nScript failed - exiting now.".format(e))
    finally:
        logging.shutdown()


if __name__ == '__main__':
    main(sys.argv[1:])
