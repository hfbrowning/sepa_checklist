import os
import multiprocessing
import sys
import csv
from main import main


def dicts_to_csv(dict_list, outfile):

    with open(outfile, 'wb+') as csv_file:  # The b in wb gets rid of the extra lines
        fieldnames = ['Sale_name', 'Agreement', 'Region_contact', 'Date_prepared', 'Auction_date',
                      'Expire_date', 'water_problem', 'Temperature', 'TEMP', 'TMDL', 'SED',
                      'Legal_description', 'Applications', 'WAU_desc', 'Slope', 'Soils',
                      'Road_miles', 'Plants', 'Animals']
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames, extrasaction='ignore')
        writer.writeheader()
        # The plural 'writerowS' is for a list of dictionaries
        writer.writerows(dict_list)


def batch_main(arg):
    base_path = arg[0]  # Everything relies upon the base path
    reader = csv.DictReader(open(os.path.join(base_path, 'sepa_checklist/tests/runner.csv')))

    # Note the true in the args list - this is for debugging purposes.
    # It means it will not write out to docx but rather to the csv
    arg_list = []
    for row in sorted(reader):
        args = (row['process_id'], row['recipient'],
                row['admin_nm'], row['ts_name'], 'True', base_path)
        arg_list.append(args)

    NUM_CORES = 8
    pool = multiprocessing.Pool(NUM_CORES, maxtasksperchild=1)
    results = pool.map(main, arg_list, chunksize=1)
    pool.close()
    pool.join()

    csv_path = os.path.join(base_path, 'sepa_checklist/tests/sanity_check_data.csv')
    dicts_to_csv(results, csv_path)


if __name__ == '__main__':
    batch_main(sys.argv[1:])