
# The purpose of this module is to house the functions that have temporarily been
# nixed from inclusion in the template. They may be included later


def find_hydromat_status(in_fc):
    """
    Get information related to hydrologic maturity status of the input unit.

    For each sub-basin that intersects in the input unit, this function calculates:

        1. The percentage of the sub-basin in the rain-on-snow (ROS) zone
        2. The percentage of the sub-basin that has achieved hydrologic maturity, and
        3. A sub-dictionary containing a "long" text snippet and a "short" Procedure
           number that describe why the the sub-basin is exempted from the Procedure.
           The focus on exemptions is because all sub-basins start out being subject
           to the Procedure and then "opt-out" by exceeding certain conditions.
           Sub-basins still subject to the Procedure return "NA" for their exemption
           reason.

    These metrics answer three separate questions in the SEPA Checklist. The entries are
    sorted by ascending order of sub-basin code.

    **Entry:** B-3-a-10 (all)

    Parameters
    ----------
    in_fc: str
        Input feature class

    Returns
    -------
    dict
        Returns a deeply nested dictionary of format:

        .. code-block:: python

            {'Hydromat':
                SliceableOrderedDict(
                {<sub_cd1>:
                    {'ROS_acres': <ros_acres>,
                    'Maturity': <maturity>,
                    'Reason': {'long': <long_answer>, 'short': <short_answer>}
                    },
                {<sub_cd1>: etc})
            }


    """
    sub_layer, _ = get_subbasins(in_fc)  # Codes not needed

    # Join the hydromat data with the subbasin info
    hydromat = os.path.join(ROPA, 'SHARED_LM.HYDROMAT_STATUS')
    arcpy.AddJoin_management(sub_layer, "SUB_CD", hydromat, "SUB_CD")

    # The following is just to simplify data access
    data_dict = {}
    with arcpy.da.SearchCursor(sub_layer, ['SHARED_LM.SUBBASIN.SUB_CD',
                                           'SHARED_LM.HYDROMAT_STATUS.ROS_ACRES',
                                           'SHARED_LM.HYDROMAT_STATUS.ROS_PCT',
                                           'SHARED_LM.HYDROMAT_STATUS.RESERVE_PCT',
                                           'SHARED_LM.HYDROMAT_STATUS.FOREST_PCT',
                                           'SHARED_LM.HYDROMAT_STATUS.KNOWN_OLD_PCT']) as cursor:
        for sub_cd, ros_acres, ros_pct, reserve_pct, forest_pct, maturity in cursor:
            data_dict[sub_cd] = {'ROS_acres': ros_acres,
                                 'Maturity': maturity,
                                 'Reason': util.procedure_na(ros_pct, reserve_pct, forest_pct)
                                 }
    new_dict = util.SlicableOrderedDict(sorted(
        {int(key): val for key, val in data_dict.items()
         }.items()))
    return {'Hydromat': new_dict}


def find_sale_acres(in_layer):
    """Function to find Sale Acres for Table A-11-a

    Nixed 6/6/18: "Until the field/regions set some standard for FMA naming we can’t effectively try to
    fill that table with any data."
    """
    # TODO - Hold off on incorporating this into template until you get answer from Patrick
    sale_acres = {row[0]: row[1] for row in arcpy.da.SearchCursor(in_layer, ["FMA_NM", "ACRES_TREATED"])}
    return {'Sale_acres': sale_acres}


def describe_water(in_fc):
    """Water is weird enough that we'll have to duplicate some of the
    functionality of the general special concerns function..."""
    wc = ("CHECKLIST LIKE '%SEPA B.3.a%' AND "
          "ISSUE_NM IN ('National Wetlands Inventory', 'Water Bodies', 'Water Courses')")
    temp_layer = 'temp_layer'
    arcpy.MakeFeatureLayer_management(in_fc, temp_layer, where_clause=wc)

    data = []
    with arcpy.da.SearchCursor(temp_layer, ["DESCRIPTION", "ISSUE_NM", "BUFFER_DISTANCE_FT"]) as cursor:
        for row in cursor:
            buffer_dict = {"buffer": row[2]}
            if row[1] == 'Water Courses':
                info = _bodies_courses(row[0], 'course')
            if row[1] == 'Water Bodies':
                info = _bodies_courses(row[0], 'body')
            if row[1] == 'National Wetlands Inventory':
                info = _wetlands(row[0])

            buffer_dict.update(info)
            data.append(buffer_dict)
    unique_list = sup.unique_and_count(data)  #sup = scripts.utilities

    if len(unique_list) == 0:
        null_dict = {'None': {'buffer': 'NA', 'count': 'NA', 'type': 'NA', 'name': 'None'}}
        return {"Water_Bodies": null_dict}
    else:
        unique_dict = {}
        for entry in unique_list:
            unique_dict[entry['name'] + '_' + entry['type']] = entry

        return {"Water_Bodies": unique_dict}


def _bodies_courses(in_row, name_type):
    """Format water bodies and water courses correctly"""
    data = sup.remove_bracket(in_row).split("|")[1:]
    replacer = 'Water {} name: '.format(name_type)
    if name_type == 'body':
        new_name = 'Unnamed {}'.format(data[-1])
    else:
        new_name = 'Unnamed water course'

    return {'name': data[0].replace(replacer, '').replace('None', new_name),
            'type': data[1].replace('State Lands type: ', '')
            }


def _wetlands(in_row):
    """Format wetlands correctly"""
    data = sup.list_to_dict(sup.remove_bracket(in_row).split("|")[1:])
    unnamed = data['Wetland_type'].lower()
    return {'name': 'Unnamed {}'.format(unnamed),
            'type': data['Cowardin_code']}



def procedure_na(ros_pct, reserve_pct, forest_pct):
    """Gives the reason why the Hydrologic Maturity Procedure does/doesn't apply.

    Per the Hydrologic Maturity Procedure :download:`(download)<_static/PR_14_004_060.pdf>` there are
    several reasons why a sub-basin in the rain-on-snow zone may not be subject to the
    Procedure. This function gives the Procedure number as well as the accompanying
    near-verbatim text snippet. This is useful for requesting either the short or long reason
    in the auto-filled SEPA checklist.

    Parameters
    ----------
    ros_pct: float
        Percentage in rain-on-snow
    reserve_pct: float
        Percentage in forest reserves
    forest_pct: float
        Percentage in long-term forest cover

    Returns
    -------
    dict
        Dictionary with a 'long' and a 'short' reason

    """
    if ros_pct < 33.33:
        return {'long': ("NO - Less than one-third of the sub-basin's area is within the rain-on-snow and " 
                         "snow-dominated zones combined (PR 14-004-060 1.b)"),
                'short': "NO - (PR 14-004-060 1.b)"}

    elif reserve_pct > 66.66:
        return {'long': ("NO - Greater than two-thirds of the sub-basin's area is within the combined rain-on-snow "
                         "and snow-dominated zones, and is covered by hydrologically mature forests, and there "
                         "is reasonable assurance that it will remain in that condition e.g. it is in a national "
                         "park, federal late-successional reserve [etc.] (PR 14-004-060 1.c)"),
                'short': "NO - (PR 14-004-060 1.c)"}

    elif forest_pct < 50.0:
        return {'long': ("NO - Less than one-half of the sub-basin is within the combined rain-on-snow and "
                         "snow-dominated zones, and is DNR-managed, and there is no reasonable assurance "
                         "that other landowners will contribute to hydrologically mature forests "
                         "(PR 14-004-060 1.d)"),
                'short': "NO - (PR 14-004-060 1.d"}

    else:
        # In this case, it *does* apply!
        return {'long': "YES - The procedure applies", 'short': "YES - The procedure applies"}

def get_subbasins(in_fc):
    """
    Find the hydrologic sub-basin(s) that intersect the input feature class.

    Parameters
    ----------
    in_fc: str
        Input feature class

    Returns
    -------
    subbasin_layer: str
        String literal 'subbasin_layer' that references a feature layer of
        the sub-basins intersecting input
    code: list
        List of sub-basin codes
    """
    subbasin_layer = 'subbasins'
    arcpy.MakeFeatureLayer_management(SUBBASIN, subbasin_layer)
    arcpy.SelectLayerByLocation_management(subbasin_layer, "INTERSECT", in_fc)
    codes = [row[0] for row in arcpy.da.SearchCursor(subbasin_layer, "SUB_CD")]
    return subbasin_layer, codes



# From utilities (nixed):

def _canonicalize_dict(x):
    """Return a (key, value) list sorted by the hash of the key.

    Used by unique_and_count as a helper function
    """
    return sorted(x.items(), key=lambda x: hash(x[0]))


def unique_and_count(list_of_dict):
    """Return a list of unique dicts with a 'count' key added


    This is useful for
    See also: https://stackoverflow.com/questions/11877747

    Parameters
    ----------
    list_of_dict : list of dictionaries with duplicate entries

    Returns
    -------
    list of unique dicts with a 'count' key added

    """
    grouper = groupby(sorted(map(_canonicalize_dict, list_of_dict)))
    return [dict(k + [("count", len(list(g)))]) for k, g in grouper]


# Archived test for unique_and_count

class TestUniqueListOfDictionaries(unittest.TestCase):
    """Note the naming convention I am attempting to follow is
    a Python-y version of Roy Osherove's suggestion:

    http://osherove.com/blog/2005/4/3/naming-standards-for-unit-tests.html

    """
    def setUp(self):
        self.test_list_of_dicts = [{'buffer': 150, 'name': 'Crater Lake', 'type': 'Lake'},
                                   {'buffer': 150, 'name': 'Crater Lake', 'type': 'Lake'}]
        self.unique_list = unique_and_count(self.test_list_of_dicts)

    def test_reduces_list_length(self):
        self.assertEqual(len(self.unique_list), 1)

    def test_adds_count_key(self):
        self.assertTrue("count" in self.unique_list[0])

    def test_counts_duplicates_correctly(self):
        """Gets the first (only) dictionary in the list and looks at its count attribute"""
        self.assertEqual(self.unique_list[0]["count"], 2)

    def test_handles_empty_lists(self):
        self.assertEqual(unique_and_count([]), [])
