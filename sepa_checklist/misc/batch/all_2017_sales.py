# Finding 2017 sales

import os
import arcpy
import csv
# This script exists to run through all of the 2017 Timber Sales in
# batch mode for testing -- don't run it again though because I think
# we're set

ROPA = '\\\\dnr\\divisions\\FR_DATA\\forest_info_2\\gis\\tools\\sde_connections_read\\ropa_gis_layer_user_direct.sde'
TIMBER_SALES = os.path.join(ROPA, 'SHARED_LRM.TS_FMA_SV')

ts_layer = 'ts_layer'
arcpy.MakeFeatureLayer_management(TIMBER_SALES, ts_layer,
                                  where_clause="FMA_STATUS_CD = 'COMPLETED' AND FMA_FY = 2017")

x = {row[0]: row[1] for row in arcpy.da.SearchCursor(ts_layer, ['TS_NM', 'ADMIN_NM'])}
with open('C:/hbro490/sepa_checklist/sepa_checklist/batch/2017_runner.csv', 'wb') as f:
    writer = csv.writer(f)
    writer.writerow(['process_id', 'recipient', 'admin_nm', 'ts_name'])
    process_id = 0
    for row in x.iteritems():
        process_id += 1
        full_row = tuple([process_id, 'Hilary.Browning@dnr.wa.gov', row[1], row[0]])
        writer.writerow(full_row)
