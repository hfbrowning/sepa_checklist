import os
import glob
import multiprocessing
import sys
import csv
import cPickle
import time
from main import main

from xlsxwriter.workbook import Workbook


def convert_multiple_csv_to_xlsx(csv_list, output_file, delete_csv=True):
    # https://stackoverflow.com/questions/17684610
    workbook = Workbook(output_file)

    for csvfile in csv_list:
        sheet_name = os.path.basename(csvfile)[:-4]
        worksheet = workbook.add_worksheet(sheet_name)
        with open(csvfile, 'rt') as f:
            reader = csv.reader(f)
            for r, row in enumerate(reader):
                for c, col in enumerate(row):
                    worksheet.write(r, c, col)
    workbook.close()
    if delete_csv:
        for csvfile in csv_list:
            try:
                os.remove(csvfile)
            except OSError:
                pass


def dicts_to_csv(dict_list, outfile):

    with open(outfile, 'wb+') as csv_file:  # The b in wb gets rid of the extra lines
        fieldnames = ['Sale_name', 'Agreement', 'Region_contact', 'Date_prepared', 'Auction_date',
                      'Expire_date', 'water_problem', 'Temperature', 'TEMP', 'TMDL', 'SED',
                      'Legal_description', 'Road_miles', 'Slope', 'Plants', 'Animals']
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames, extrasaction='ignore')
        writer.writeheader()
        # The plural 'writerowS' is for a list of dictionaries
        writer.writerows(dict_list)


def rm_nested_dict(full_dict, nest_name):
    """
    This function does dual duty: it modifies
    the full dictionary at the same time it
    returns the nested dictionary for separate
    use.

    It also links it back to the original sale
    name so we don't lose that information.

    Parameters
    ----------
    full_dict
    nest_name

    Returns
    -------

    """
    try:
        x = full_dict.pop(nest_name)
        for k, v in x.items():
            v.update({'Sale_name': full_dict['Sale_name']})
        return x
    except KeyError:
        pass


def write_subdict_csv(input_list, first_field, outfile):

    # From the first dictionary in the list get the key names
    # associated with the sub-dictionary. These are the field
    # names

    # Note: every field but the first will be in random order
    # because it's a dictionary. That's okay - the data follows
    # the column name as it should
    fields = input_list[0].values()[0].keys()
    fields.insert(0, first_field)

    with open(outfile, 'wb') as f:
        w = csv.DictWriter(f, fieldnames=fields)
        w.writeheader()
        for entry in input_list:
            for key, val in sorted(entry.items()):
                row = {first_field: key}
                row.update(val)
                w.writerow(row)


def master_write_out(results_list, output_path):

    # This is all rather repetitive but I just don't care...
    app_list = []
    wau_list = []
    soil_list = []

    for element in results_list:
        app_list.append(rm_nested_dict(element, 'Applications'))
        wau_list.append(rm_nested_dict(element, 'WAU_desc'))
        soil_list.append(rm_nested_dict(element, 'Soils'))

    csv_list = [os.path.join(output_path, x) for x in ['wau_desc.csv', 'applications.csv',
                                                       'soil.csv', 'main_list.csv']]
    write_subdict_csv(wau_list,
                      first_field='WAU', outfile=csv_list[0])
    write_subdict_csv(app_list,
                      first_field='WAU', outfile=csv_list[1])
    write_subdict_csv(soil_list,
                      first_field='Acres', outfile=csv_list[2])
    dicts_to_csv(results_list, outfile=csv_list[3])

    xlsx_name = os.path.join(output_path, 'all.xlsx')
    convert_multiple_csv_to_xlsx(csv_list, xlsx_name, delete_csv=False)


def batch_process_main(arg):
    base_path = arg[0]  # Everything relies upon the base path
    runner_script = arg[1]
    reader = csv.DictReader(open(runner_script))

    # Note the true in the args list - this is for debugging purposes.
    # It means it will not write out to docx but rather to the csv
    arg_list = []
    for row in sorted(reader):
        args = (row['process_id'], row['recipient'],
                row['admin_nm'], row['ts_name'], 'True', base_path)
        arg_list.append(args)

    NUM_CORES = 7
    pool = multiprocessing.Pool(NUM_CORES, maxtasksperchild=1)
    results = pool.map(main, arg_list, chunksize=1)
    print("Sleeping before closing the pool! (Multiprocessing Issue #10332)")
    time.sleep(30)
    pool.close()
    pool.join()

    with open(os.path.join(base_path, r'sepa_checklist\2017_runner.p'), "wb") as handle:
        cPickle.dump(results, handle, protocol=cPickle.HIGHEST_PROTOCOL)

    # csv_folder = os.path.join(base_path, r'sepa_checklist\sepa_checklist\batch\csv')
    # master_write_out(results, csv_folder)
    #
    # csv_path = os.path.join(base_path, 'sepa_checklist/tests/sanity_check_data.csv')
    # dicts_to_csv(results, csv_path)


if __name__ == '__main__':
    batch_process_main(sys.argv[1:])
