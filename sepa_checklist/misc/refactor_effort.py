
# Eventually it would be nice to replace the monstrosity of the get_fp_applications
# function with a class but I'll worry about it latler


class ForestPracticesApplications(object):

    DAYS_IN_YEAR = 365.25
    FUTURE_YEARS = 7   # "Future is defined as occurring within next 5-7 years"

    def __init__(self, in_fc):
        self.in_fc = in_fc
        self.current_date = datetime.datetime.today().replace(minute=0, hour=0, second=0, microsecond=0)
        self.future_date = self.current_date + datetime.timedelta(DAYS_IN_YEAR * FUTURE_YEARS)
        self.wau_layer, self.wau_list = get_wau(self.in_fc)
        self.clauses = {'DNR_Even': self._partial_clause(operator='>'),
                        'DNR_Uneven': self._partial_clause(operator='=')
                        }

    # The two dnr clauses are almost identical - trying to avoid lots of string duplication!
    def _partial_clause(self, operator):
        return ("(SURFACE_TRUST_CD {op} 0 OR TIMBER_TRUST_CD {op} 0) AND "
                "FP_ID > 0 AND DECISION = 'APPROVED' AND "
                "CUTTING_OR_REMOVING_TIMBER_FLG = 'Y' AND "
                "TIMHARV_FP_TY_LABEL_NM = 'EVEN-AGE' AND "
                "EFFECTIVE_DT >= date '{current}' AND "
                "EFFECTIVE_DT < date '{future}'").format(op=operator,
                                                         current=self.current_date,
                                                         future=self.future_date)


#     'Non_DNR': ("(SURFACE_TRUST_CD = 0 OR TIMBER_TRUST_CD = 0) AND "
#                                 "FP_ID > 0 AND DECISION = 'APPROVED' AND "
#                                 "CUTTING_OR_REMOVING_TIMBER_FLG = 'Y' AND "
#                                 "EFFECTIVE_DT > date '{}'").format(TEST_DATE))
#
#
# }


