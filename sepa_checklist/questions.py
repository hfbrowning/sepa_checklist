import datetime
import logging
import os
import time

import arcpy
import docxtpl

import data
import util
from arcpy_logging import ArcpyLog

DAYS_IN_YEAR = 365.25

log = logging.getLogger(__name__)
alog = ArcpyLog(log, log_level='DEBUG')


# ROPA = '\\\\dnr\\divisions\\FR_DATA\\forest_info_2\\gis\\tools\\sde_connections_read\\ropa_gis_layer_user_direct.sde'
ROPA2 = '\\\\dnr\\divisions\\FR_DATA\\forest_info_2\\gis\\tools\\sde_connections_read\\ropa_gis_layer_user_direct.sde'
# RASTER = '\\\\dnr\\divisions\\FR_DATA\\forest_info_2\\gis\\tools\\sde_connections_read\\raster_gis_layer_user_direct.sde'

connect_home = os.getenv("CONNECTHOME")   # Appworx batch
ropa_instance = os.getenv("ROPA_SDE_INSTANCE")   # Appworx batch
raster_instance = os.getenv("RASTER_SDE_INSTANCE")  # Appworx batch
ROPA = connect_home + "\\" + ropa_instance + "_sepamaps_user.sde"
RASTER = connect_home + "\\" + raster_instance + "_sepamaps_user.sde"

WAUS = os.path.join(ROPA, 'ROPA.WAU_SV')
ROADS = os.path.join(ROPA, 'ROPA.ROAD')
SLOPE = os.path.join(RASTER, 'RASTER.SLOPE_PERCENT_10M')
LEGAL_INFO = os.path.join(ROPA, 'CADASTRE.TWPSUBDIV_SV')
ROS = os.path.join(ROPA, 'ROPA.ROS')
SUBBASIN = os.path.join(ROPA, 'SHARED_LM.SUBBASIN')
DNR_LAND = os.path.join(ROPA, 'ROPA.PARCEL_SV')
FP_APPLICATIONS = os.path.join(ROPA, 'ROPA.FP_APP_HARVEST_SV_MV')
PRECIP = os.path.join(ROPA, 'ROPA.PRECIP')
ELEV_RASTER = os.path.join(RASTER, 'RASTER.DEM_10M')

VEG_ZONES = os.path.join(ROPA2, 'SHARED_LM.VEG_ZONES')

OVERLAY = os.path.join(ROPA, 'SHARED_LRM.XREF_OVL_FMA_SC')
TIMBER_SALES = os.path.join(ROPA, 'SHARED_LRM.TS_FMA_SV')

# General Arcpy functions
# ---------------------------------------------------------


def calculate_acreage(in_fc):
    """
    Add ACRES field if necessary and re-calculate acreage.

    Parameters
    ----------
    in_fc: str
        Feature class to modify

    Returns
    -------
    None
    """
    if "ACRES" not in [field.name for field in arcpy.ListFields(in_fc)]:
        arcpy.AddField_management(in_fc, "ACRES", field_type="DOUBLE")
    arcpy.CalculateField_management(in_fc, "ACRES", "!SHAPE.AREA@ACRES!", "PYTHON_9.3")
    alog.log()


def safe_delete_memory(input_layer):
    """
    Delete an in_memory feature layer if it exists.

    This function is used to clean up the in_memory workspace at the end
    of functions. It logs a warning message if the input_layer does not
    exist, since this is unexpected behavior.

    Parameters
    ----------
    input_layer: str
        in_memory feature layer

    Returns
    -------
    None
        Delete a layer (side effect)

    """
    try:
        arcpy.Delete_management(input_layer)
    except (NameError, arcpy.ExecuteError):
        log.warning('in_memory layer {} did not exist - could not delete'.format(
            input_layer)
        )
        pass

# SEPA Question Functions:
#   Arranged in order of their appearance on the checklist
#   Single underline = helper function not on list directly
#   These come first in order
# ---------------------------------------------------------


def _legal_description(in_layer, gdb_path):
    """
    Find the legal description(s) overlapping a timber sale.

    **Entry:** A-12-a

    Parameters
    ----------
    in_layer: str
        Feature layer corresponding to timber sale of interest
    gdb_path: str
        Geodatabase to write temporary feature class to

    Returns
    -------
    str
        Comma-joined string of legal descriptions
    """
    temp_fc = 'in_memory/temp_fc_legal'
    arcpy.Intersect_analysis([in_layer, LEGAL_INFO], temp_fc)
    alog.log()
    descriptions = set([row[0] for row in arcpy.da.SearchCursor(temp_fc, "LEGAL_DESC_NM")])
    arcpy.Delete_management(temp_fc)
    return ", ".join(descriptions)


def simple_info(in_layer, gdb_path):
    """
    A collection of 'simple' information for the SEPA Checklist.

    **Entries:**

        * A-1 (sale name/agreement number)
        * A-4 (date prepared)
        * A-6 (auction date/expiration date)
        * A-12-a (legal description)

    Parameters
    ----------
    in_layer: str
        Feature layer corresponding to timber sale of interest
    gdb_path: str
        Geodatabase to write temporary feature class to

    Returns
    -------
    dict
        Dictionary of simple info entries
    """

    def _first_entry(field):
        """This works because it never gets called if the in_layer is None :)"""
        return next((row[0] for row in arcpy.da.SearchCursor(in_layer, field)), 'None')

    current_date = time.strftime("%m/%d/%Y")
    if in_layer:
        sale_name = _first_entry("TS_NM")
        legal_desc = _legal_description(in_layer, gdb_path)
        agreement = _first_entry("TS_CONTRACT_NO")
        auction_date = util.better_strftime(_first_entry("TS_DT"))
        expiration_date = util.better_strftime(_first_entry("TS_EXPIRE_DT"))
    else:
        sale_name = None
        legal_desc = None
        agreement = "Not yet available"
        auction_date = None
        expiration_date = None

    # Last ditch effort get a derived expiration date, if possible
    if expiration_date is None:
        try:
            FUTURE_YEARS = 2
            auction_format = datetime.datetime.strptime(auction_date,
                                                        '%m/%d/%Y').date()
            expr_date = auction_format + datetime.timedelta(DAYS_IN_YEAR * FUTURE_YEARS)
            expiration_date = util.better_strftime(expr_date)
        except TypeError:
            pass
    log.info(("Simple info calculated: \n"
              "Sale Name: {0}\n"
              "Legal Desc: {1}\n"
              "Agreement Num: {2}\n"
              "Auction Date: {3}\n"
              "Expiration Date: {4}\n").format(sale_name, legal_desc, agreement, auction_date, expiration_date))
    return {"Sale_name": sale_name,
            "Legal_description": legal_desc,
            "Agreement": agreement,
            "Auction_date": auction_date,
            "Expire_date": expiration_date,
            "Date_prepared": current_date}


def find_region_contact(in_layer):
    """
    Find the correct regional office contact.

    **Entry:** A-3

    Parameters
    ----------
    in_layer: str
        Feature layer corresponding to timber sale of interest

    Returns
    -------
    dict
        Dict with properly formatted string block for region contact.
    """
    region = next(row[0] for row in arcpy.da.SearchCursor(in_layer, ["REGION_NM"]))
    # Note: docxtpl.R() honors new line characters in text
    log.info("Region contact found: {}".format(region))
    return {'Region_contact': docxtpl.R(data.REGION_CONTACTS[region])}


def _special_concerns(where_clause, input_fc, acres=False):
    """
    General pattern for most Special Concerns-related functions.

    Pattern:

        1. Make a subset of data based upon the SEPA checklist or other field
        2. Parse the description field of the subset
        3. Additionally calculate acres if needed

    Parameters
    ----------
    where_clause: str
        SQL query
    input_fc: str
        Special Concerns feature class
    acres: bool
        If True, calculate acres

    Returns
    -------
    data: list
        A list of data elements that will need to be further parsed by util.list_to_dict()

    """
    temp_layer = 'temp_layer'
    arcpy.MakeFeatureLayer_management(input_fc, temp_layer,
                                      where_clause=where_clause)
    data = []
    count = 0
    log.debug("Special Concerns where_clause: {}".format(where_clause))
    # Calculating acres makes it look more complicated :)
    if acres:
        calculate_acreage(temp_layer)
        with arcpy.da.SearchCursor(temp_layer, ["DESCRIPTION", "ACRES"]) as cursor:
            for row in cursor:
                acre_entry = "Acres: {}".format(row[1])
                try:
                    new_entry = util.remove_bracket(row[0]).split("|") + [acre_entry]
                    data.append(new_entry)
                    count += 1
                    log.debug("SC row {} is {}".format(count, new_entry))
                except AttributeError:
                    pass
    else:
        with arcpy.da.SearchCursor(temp_layer, ["DESCRIPTION"]) as cursor:
            for row in cursor:
                try:
                    new_entry = util.remove_bracket(row[0]).split("|")
                    data.append(new_entry)
                    count += 1
                    log.debug("SC row {} is {}".format(count, new_entry))
                except AttributeError:
                    pass
    return data


def find_303d_problems(in_fc):
    """
    Find any 303-d water issues in the feature class.

    303(d) refers to the subsection of the Clean Water Act concerned with
    Impaired Waters and Total Maximum Daily Loads (TMDLs). This function finds
    issues listed in Special Concerns for a given spatial area; these could be
    any of Temperature (or 'TEMP'), Sediment ('SED'), or an unspecified other
    'TMDL'. If any of these particular issues is yes, then the overall problem
    box in the Special Concerns checklist also gets checked.

    **Entry:** A-8

    Parameters
    ----------
    in_fc: str
        Input feature class

    Returns
    -------
    dict
        Returns a dictionary of the format::

            {'SED': <true/false>,
             'TEMP': <true/false>,
             'TMDL': <true/false>,
             'water_problem': <true/false>}


    """
    wc = "ISSUE_NM = '303(d) Listed Waterbodies'"
    data = _special_concerns(wc, in_fc)
    data_list = [util.list_to_dict(y) for y in data]

    problem_list = ["TEMP", "TMDL", "SED"]
    return_dict = {}
    for problem in problem_list:
        return_dict[problem] = any(x['Issue'] == problem for x in data_list)

    # Checking to see if >0 303D problems
    return_dict['water_problem'] = any(val for key, val in return_dict.items())
    log.info("303d problems dictionary: {}".format(return_dict))
    return return_dict


def _delete_dupes(in_corporate, unique_fields_list, gdb_path):
    """
    Make a copy of input feature class with unique rows based on specified fields.

    A common problem with "corporate" (or other) data is that it may have duplicate rows,
    especially when considering only a few fields of interest. For example, consider:

    +----------+--------+-----------+-----------+
    | OBJECTID | REGION | SOIL_TYPE | SEED_ZONE |
    +==========+========+===========+===========+
    | 1        |Olympic | Loam      |  PSME04   |
    +----------+--------+-----------+-----------+
    | 2        |Olympic | Silt      |  PSME04   |
    +----------+--------+-----------+-----------+
    | 3        |Olympic | Clay      |  PSME09   |
    +----------+--------+-----------+-----------+


    Specifying ``unique_field_list=['REGION']`` would yield only a single entry in the
    resulting table, whereas ``unique_field_list=['REGION', 'SEED_ZONE']`` would yield two.
    In no circumstance are we likely to want to include a unique field like ``OBJECTID``.

    Parameters
    ----------
    in_corporate: str
        Full path to data source to de-duplicate
    unique_fields_list: list
        List of fields to de-duplicate on
    gdb_path: str
        Geodatabase where output feature class should be saved

    Returns
    -------
    temp_fc: str
        Full path on disk to output feature class

    """
    temp_fc = os.path.join(gdb_path, 'deduped_fp_apps')

    # This strategy is slow but works (tried to fix it in commit
    # hash 377efd2db88aa376c66003263c5c0a63ad05852d)
    arcpy.CopyFeatures_management(in_corporate, temp_fc)
    temp_set = set()
    with arcpy.da.UpdateCursor(temp_fc, unique_fields_list) as cursor:
        for row in cursor:
            if tuple(row) in temp_set:
                cursor.deleteRow()
            else:
                temp_set.add(tuple(row))
    log.debug('FP Apps deduplicated')
    return temp_fc


def _get_acres(in_layer, wc=None, digits=1):
    """
    Retrieve the total acres from a feature class or feature layer.

    Note
    ----
    This function cannot handle an input layer that lacks an 'ACRES' field.
    This is by design to avoid re-calculating acreage every time, which can be time-consuming.
    Therefore it requires the user exercise due diligence.

    Parameters
    ----------
    in_layer: str
        Input feature layer/feature class
    wc: str, optional
        Optional SQL query to subset the input before calculating acres
    digits: int
        The number of digits to round the output to

    Returns
    -------
    float
        Total acres, rounded
    """
    return round(sum([row[0] for row in arcpy.da.SearchCursor(in_layer, 'ACRES', wc)]), digits)


def _get_dnr_acres(in_layer, gdb_path):
    """
    Retrieve the total acres of DNR surface trust land from a feature class or feature layer.

    Parameters
    ----------
    in_layer: str
        Input feature layer/feature class

    gdb_path: str
        Geodatabase path to write temporary de-duplicated file out to

    Returns
    -------
    float
        Total DNR acres, rounded
    """
    dnr_unique = _delete_dupes(in_layer, unique_fields_list=['SHAPE_Area', 'SHAPE_Length'], gdb_path=gdb_path)
    return _get_acres(dnr_unique, wc="SURFACE_TRUST_CD > 0 AND PARCEL_TYPE_CD = 1")


def _get_dnr_ts_acres(wau, future_date, even_age=True):
    """
    Retrieve the even/uneven-aged future harvest on DNR trust land for a WAU.

    Parameters
    ----------
    wau: str
        String corresponding to WAU_NM
    future_date: str
        Today's date + 7 years in the future
    even_age: bool
        If true get even-aged stands, otherwise uneven-age stands

    Returns
    -------
    float
        Future harvest acreage, rounded
    """

    # Make mini layer from individual wau
    wau_layer = 'wau_layer'
    arcpy.MakeFeatureLayer_management(WAUS, wau_layer,
                                      where_clause="WAU_NM='{}'".format(wau))
    alog.log()
    # If you want uneven age, modify the wc to avoid the small set of even age types
    if even_age:
        codes = "{}".format(tuple(data.EVEN_CODES))
    else:
        codes = "{}".format(tuple(data.UNEVEN_CODES))
    wc = ("FMA_STATUS_CD = 'PLANNED' "
          "AND FMA_DT < timestamp '{future}' "
          "AND TECHNIQUE_CD IN {codes}").format(future=future_date, codes=codes)
    ts_layer = 'ts_layer'
    arcpy.MakeFeatureLayer_management(TIMBER_SALES, ts_layer)
    arcpy.SelectLayerByLocation_management(ts_layer,
                                           "INTERSECT",
                                           wau_layer,
                                           selection_type="ADD_TO_SELECTION")
    # Check that all of the technique codes are known
    # Will simply raise an exception if it fails within the timber sale
    x = set([row[0] for row in arcpy.da.SearchCursor(ts_layer, 'TECHNIQUE_CD')])
    util.check_corporate_techniques(x)
    arcpy.SelectLayerByAttribute_management(ts_layer,
                                            selection_type="SUBSET_SELECTION",
                                            where_clause=wc)
    alog.log()
    # Then clip to get nice edges
    temp_layer = 'in_memory/timber_sales'
    arcpy.Clip_analysis(ts_layer, wau_layer, temp_layer)
    calculate_acreage(temp_layer)
    result = round(sum((row[0] for row in arcpy.da.SearchCursor(temp_layer, "ACRES"))), 1)

    safe_delete_memory(temp_layer)
    return result


def _get_wau(in_fc):
    """
    Find the Watershed Analysis Units (WAUs) that intersect the input feature class.

    Parameters
    ----------
    in_fc: str
        Input feature class

    Returns
    -------
    wau_layer: str
        String literal 'wau_layer' that references a feature layer of the WAUs
        intersecting input
    wau_list: list
        List of overlapping WAUs
    """
    wau_layer = 'wau_layer'
    arcpy.MakeFeatureLayer_management(WAUS, wau_layer)
    alog.log()
    arcpy.SelectLayerByLocation_management(wau_layer, "INTERSECT", in_fc)
    alog.log()
    wau_set = set(x[0] for x in arcpy.da.SearchCursor(wau_layer, "WAU_NM"))
    return wau_layer, list(wau_set)


def get_fp_applications(in_fc, gdb_path):
    """
    Get information related to approved Forest Practices Applications.

    This is a wrapper function that pulls together, for each of the WAUs in a given
    input feature class, the:

        1. Total acreage of the WAU that overlaps the feature class
        2. The total surface trust acres managed by DNR
        3. The amount of those acres from (2) that have been approved for even-aged
           harvest in the next 7 years
        4. The amount of those acres from (2) that have been approved for uneven-aged
           harvest in the next 7 years
        5. The total acres managed by entities other than DNR that have been
           granted a Forest Practices application for the next 7 years

    **Entry:** A-13-e (table)

    Parameters
    ----------
    in_fc: str
        Input feature class
    gdb_path: str
        Geodatabase path for writing temporary outputs

    Returns
    -------
    dict
        Returns a deeply nested dictionary of format:

        .. code-block:: python

            {'Applications':
                {<wau1> :
                    {'Acres: <acres>,
                     'DNR_Acres': <dnr_acres>,
                     'DNR_Even': <dnr_even>,
                     'DNR_Uneven': <dnr_uneven>,
                     'Non_DNR': <non_dnr>
                    },
                {<wau2>: etc}
            }


    """
    current_date = datetime.datetime.today().replace(minute=0, hour=0, second=0, microsecond=0)
    FUTURE_YEARS = 7  # "Future is defined as occurring within next 7 years"
    future_date = current_date + datetime.timedelta(DAYS_IN_YEAR * FUTURE_YEARS)
    log.info('In FP apps function. Current date: {}, Future date: {}'.format(current_date, future_date))

    wau_layer, wau_list = _get_wau(in_fc)
    arcpy.MakeFeatureLayer_management(DNR_LAND, 'dnr_land')
    arcpy.SelectLayerByLocation_management('dnr_land', "INTERSECT", wau_layer)
    alog.log()

    # Deduplicate the FP applications - because there are a lot of duplicates!
    deduped_apps = _delete_dupes(FP_APPLICATIONS, unique_fields_list=['FP_ID', 'SHAPE_Area', 'TIMHARV_FP_TY_LABEL_NM'],
                                gdb_path=gdb_path)
    arcpy.MakeFeatureLayer_management(deduped_apps, 'fp_apps')
    arcpy.SelectLayerByLocation_management('fp_apps', "INTERSECT", wau_layer)
    alog.log()

    # kj - union_layer, union_layer2 = 'in_memory/union_layer', 'union_layer'
    union_layer, union_layer2 = "in_memory\\union_layer", "union_layer"
    arcpy.Union_analysis([wau_layer, 'fp_apps', 'dnr_land'], union_layer)
    alog.log()

    # Delete entries outside of the WAU
    with arcpy.da.UpdateCursor(union_layer, "FID_WAU_SV") as cursor:
        for row in cursor:
            if row[0] == -1:
                cursor.deleteRow()
    # Need to use union_layer2 here because of issues with in_memory (it is considered
    # not a layer?)
    arcpy.MakeFeatureLayer_management(union_layer, union_layer2)
    calculate_acreage(union_layer2)

    table_dict = {}
    for wau in wau_list:
        log.debug('FP Apps run for WAU = {}'.format(wau))
        arcpy.SelectLayerByAttribute_management(union_layer2, where_clause="WAU_NM='{}'".format(wau))
        table_dict[wau] = {'Acres': int(round(next((row[0] for row in arcpy.da.SearchCursor(union_layer2,
                                                                                        "WAU_ACRES")),
                                               'None'), 0))}
        table_dict[wau]['DNR_Acres'] = int(round(_get_dnr_acres(union_layer2, gdb_path=gdb_path), 0))

        # Queries...
        table_dict[wau]['DNR_Even'] = int(round(_get_dnr_ts_acres(wau, future_date, even_age=True), 0))

        table_dict[wau]['DNR_Uneven'] = int(round(_get_dnr_ts_acres(wau, future_date, even_age=False), 0))

        table_dict[wau]['Non_DNR'] = int(round(_get_acres(union_layer2,
                                                      wc=("PARCEL_TYPE_CD <> 1 AND "
                                                          "FP_ID > 0 AND DECISION = 'APPROVED' AND "
                                                          "CUTTING_OR_REMOVING_TIMBER_FLG = 'Y' AND "
                                                          "EXPIRATION_DT > date '{}'").format(current_date)),
                                           0))
    safe_delete_memory(union_layer)
    return {'Applications': table_dict}


def _get_elev_statistics(wau_layer, gdb_path):
    """
    Get the range and mean of elevation for a WAU.

    Parameters
    ----------
    wau_layer: str
        Feature layer of a single WAU
    gdb_path: str
        Place on disk to write out temp raster products

    Returns
    -------
    dict
        Dictionary of min, max, and mean elevation.
    """
    log.info("Getting elevation statistics")

    geom_obj = next(row[0] for row in arcpy.da.SearchCursor(wau_layer, ["SHAPE@"]))
    arcpy.env.extent = geom_obj.extent
    out_raster = os.path.join(gdb_path, 'raster_elev_clip')
    arcpy.Clip_management(ELEV_RASTER, '#', out_raster, geom_obj, '#', 'ClippingGeometry')
    out_ras_obj = arcpy.Raster(out_raster)

    # Min can be less than 0 - but we don't want that since it
    # may confuse users
    if int(out_ras_obj.minimum) < 0:
        elev_min = 0
    else:
        elev_min = int(out_ras_obj.minimum)

    return_dict = {'Elev_Min': elev_min,
                   'Elev_Max': int(out_ras_obj.maximum),
                   'Elev_Mean': int(out_ras_obj.mean)}
    arcpy.ClearEnvironment('extent')
    safe_delete_memory(out_raster)
    return return_dict


def _get_mean_precip(in_wau_layer):
    """
    Get the weighted average precipitation for a WAU.

    Parameters
    ----------
    in_wau_layer: str
        Feature layer of a single WAU

    Returns
    -------
    int
        Weighted average precip as integer
    """
    temp_layer = 'in_memory/precip'
    arcpy.Clip_analysis(PRECIP, in_wau_layer, temp_layer)
    log.info('Getting mean precip')
    alog.log()
    calculate_acreage(temp_layer)

    # Calculate the weighted average of precipitation
    total_acres = sum(x[0] for x in arcpy.da.SearchCursor(temp_layer, ['ACRES']))
    weight_avg = sum((x*y)/total_acres for x, y in arcpy.da.SearchCursor(temp_layer,
                                                                        ['PRECIP_AVG_DPT',
                                                                        'ACRES']))
    safe_delete_memory(temp_layer)
    return int(round(weight_avg, 0))


def _get_veg_zone(wau_layer, gdb_path):
    """
    Get the most common vegetation type for a WAU.

    Parameters
    ----------
    wau_layer: str
        Feature layer of a single WAU
    gdb_path: str
        Place on disk to write out temp raster product

    Returns
    -------
    str
        Most common vegetation type (i.e., "Mountain Hemlock", etc.)

    """
    log.info("Getting majority veg zones")
    geom_obj = next(row[0] for row in arcpy.da.SearchCursor(wau_layer, ["SHAPE@"]))
    arcpy.env.extent = geom_obj.extent
    out_raster = os.path.join(gdb_path, 'raster_veg_clip')
    arcpy.Clip_management(VEG_ZONES, '#', out_raster, geom_obj, '#', 'ClippingGeometry')

    sorted_veg = sorted(arcpy.da.SearchCursor(out_raster, ["COUNT", "VEG_ZONE"]), reverse=True)
    most_common = sorted_veg[0][1]

    arcpy.ClearEnvironment("extent")
    safe_delete_memory(out_raster)
    return most_common.title()


def get_wau_description(in_fc, gdb_path):
    """
    Find the general description of the WAU.

    This is a wrapper function that brings together many helper functions to
    find the "general" description of the WAU, e.g., the acreage, elevation,
    precipitation and primary forest vegetation zone of the WAU.

    **Entry:** B-1-a-1

    Parameters
    ----------
    in_fc: str
        Input feature class
    gdb_path: str
        Place on disk to write out temp raster products

    Returns
    -------
    dict
       Return a dictionary of format:

        .. code-block:: python

            {'WAU_desc':
                {<wau_nm>:
                    {'Acres': <acres>,
                     'Elev_Max': <elev_max>,
                     'Elev_Mean': <elev_mean>,
                     'Elev_Min': <elev_min>,
                     'Mean_Precip': <mean_precip>,
                     'Veg_Zone': <veg_zone>},
                {<wau_nm2>: etc}
            }

    """
    description_dict = {}
    _, wau_list = _get_wau(in_fc)  # Don't need layer right now
    log.info("Getting WAU descriptions")
    for wau in wau_list:
        wau_name = wau.replace(' ', '').replace('/', '')
        log.debug("Old wau name = {}, new wau name = ".format(wau, wau_name))
        layer_name = 'layer_{}'.format(wau_name)
        arcpy.MakeFeatureLayer_management(WAUS, layer_name,
                                          where_clause="WAU_NM='{}'".format(wau))
        description_dict[wau] = {'Mean_Precip': _get_mean_precip(layer_name)}
        description_dict[wau].update(_get_elev_statistics(layer_name, gdb_path))
        description_dict[wau].update({'Veg_Zone': _get_veg_zone(layer_name, gdb_path)})
        wau_acres = int(round(next(row[0] for row in arcpy.da.SearchCursor(layer_name, "WAU_ACRES")), 0))
        description_dict[wau].update({'Acres': wau_acres})
    return {'WAU_desc': description_dict}


def find_slope(in_fc, gdb_path):
    """
    Find the steepest slope within the unit.

    Using the USGS 10m slope model, this function retrieves the steepest slope that
    occurs within the timber sale unit.

    **Entry:** B-1-b

    Parameters
    ----------
    in_fc: str
        Input feature class
    gdb_path: str
        Path to write temporary raster clip to

    Returns
    -------
    dict
        Returns a dictionary of the format::

            {'Slope': <max_slope>}

    """
    log.info("Getting maximum slope")
    arcpy.env.extent = in_fc
    out_raster = os.path.join(gdb_path, 'raster_slope_clip')
    arcpy.Clip_management(SLOPE, '#', out_raster, in_fc, '#', 'ClippingGeometry')
    out_ras_obj = arcpy.Raster(out_raster)
    raster_max = int(out_ras_obj.maximum)
    arcpy.ClearEnvironment('extent')
    safe_delete_memory(out_raster)
    return {'Slope': raster_max}


def describe_soils(in_fc, top_n=10, acres=True):
    """
    Get the top N soil species in the input fc sorted by acre.

    This function returns data intended for a table which could get
    quite long; therefore it's helpful to limit it to 10 (or however
    many) entries. In addition the output contains many more details
    than are being used at the time of writing, but provides them at
    "no additional cost".

    **Entry:** B-1-c

    Parameters
    ----------
    in_fc: str
        Input feature class
    top_n: int
        The number of entries to return
    acres: bool
        If true, calculate and return acres

    Returns
    -------
    dict
        Return a Sliceable Ordered Dictionary of format:

        .. code-block:: python

            {'Soils':
                SliceableOrderedDict(
                {<acres1>:
                    {'Survey_Number': <survey_num>,
                     'Texture': <texture>,
                     'Acres': <acres>},
                {<acres2>: etc})
            }


    """
    wc = "ISSUE_NM = 'Soil'"
    data = _special_concerns(wc, in_fc, acres)

    null_return = {'Soils': {'NA': {'Texture': 'NA', 'Survey_Number': 'NA', 'Acres': 'NA'}}}
    if not data:
        # Some spatial locations have no corporate soils data, so 'data' will
        # be an empty list
        log.info("No soils data")
        return null_return
    else:
        formatted = util.format_soils(data,
                                      survey_str='Survey_Number',
                                      acre_str='Acres',
                                      texture_str='Texture')
        if not formatted:
            # If all textures are none you will also get an empty list
            log.info("No soils data")
            return null_return

        # Create an ordered dictionary with acres as the key
        # that can be sliced - this allows us to remove all but N entries
        new_dict = util.SlicableOrderedDict(sorted(
            {float(val["Acres"]): val for val in formatted}.items(),
            reverse=True))
        return {'Soils': new_dict[0:top_n]}


def find_road_miles(in_fc):
    """
    Calculate the miles of road in the unit by WAU.

    For each portion of a WAU that intersects the input unit, this function
    will calculate and return the number of road miles per square mile of land.

    **Entry:** B-3-a-8

    Parameters
    ----------
    in_fc: str
        Input feature class

    Returns
    -------
    dict
        Returns a nested dictionary of format:

        .. code-block:: python

            {'Road_miles': {<wau1>: <miles>,
                            <wau2>: <miles>,
                            etc}


    """
    wau_layer, wau_list = _get_wau(in_fc)
    roads_layer = 'roads_layer'
    arcpy.MakeFeatureLayer_management(ROADS, roads_layer,
                                      where_clause="ROAD_STATUS_NM NOT IN ('FP Abandoned', 'Planned')")
    arcpy.SelectLayerByLocation_management(roads_layer, "INTERSECT", wau_layer)
    intersect_layer = 'in_memory/intersect_layer'
    arcpy.Intersect_analysis([roads_layer, wau_layer], intersect_layer)

    wau_dict = {}
    log.info("Getting WAU road miles")
    for wau in wau_list:
        wau_name = wau.replace(' ', '').replace('/', '')
        log.debug("Old wau name = {}, new wau name = ".format(wau, wau_name))
        temp_layer = 'layer_{}'.format(wau_name)
        arcpy.MakeFeatureLayer_management(intersect_layer, temp_layer, "WAU_NM = '{}'".format(wau))
        road_miles = util.ft_to_miles(sum([row[0] for row in arcpy.da.SearchCursor(temp_layer, "SHAPE@Length")]))
        wau_acres = next(row[0] for row in arcpy.da.SearchCursor(temp_layer, "WAU_ACRES"))
        wau_miles = util.acres_to_sq_miles(wau_acres)
        wau_dict[wau] = "{} (mi./sq. mi.)".format(round(road_miles / wau_miles, 1))
    safe_delete_memory(intersect_layer)
    return {'Road_miles': wau_dict}


def _ESA_special_concerns(ts_layer, wc):
    """
    ESA-specific analog to special concerns.

    Parse the Special Concerns pre-generated overlay table for endangered
    plants and animals. A more efficient alternative to overlaying Special
    Concerns again using ``_special_concerns`` function.

    Parameters
    ----------
    ts_layer: str
        Timber sale feature layer
    wc: str
        SQL query (where clause)

    Returns
    -------
    data: list
        A list of data elements that will need to be further parsed by util.list_to_dict()
    """

    FMA_map = {row[0]: row[1] for row in arcpy.da.SearchCursor(ts_layer, ["FMA_ID", "FMA_NM"])}
    log.debug("This is the FMA map for ESA Special Concerns overlay: {}".format(FMA_map))
    table_view = 'table_view'
    arcpy.MakeTableView_management(OVERLAY,
                                   table_view,
                                   where_clause="{0} AND FMA_ID in ({1})".format(
                                       wc,
                                       ','.join(str(x) for x in FMA_map.keys())))
    alog.log()
    data = []
    count = 0
    with arcpy.da.SearchCursor(table_view, ["DESCRIPTION", "ACRES_OVERLAP", "FMA_ID"]) as cursor:
        for row in cursor:
            acres = "Acres: {}".format(row[1])
            fma_nm = "FMA_NM: {}".format(FMA_map[row[2]])
            try:
                # Split on carriage return
                new_entry = util.remove_bracket(row[0]).splitlines() + [acres] + [fma_nm]
                data.append(new_entry)
                count += 1
                log.debug("SC row {} is {}".format(count, new_entry))
            except AttributeError:
                pass
    arcpy.Delete_management(table_view)
    return data


def _find_endangered(in_layer, wc, return_type, status_list):
    """
    Base function for finding data related to endangered species.

    This function is called by ``find_endangered_plants`` and
    ``find_endangered_animals`` and is responsible for doing the 'heavy
    lifting'. The differences between these two functions are minimal -
    simply different SQL queries and key words to define ESA status.

    Parameters
    ----------
    in_fc: str
        Input feature class
    wc: str
        SQL query
    return_type: str
        Options are 'Plants' or 'Animals'
    status_list: list
        List of status key words to search for

    Returns
    -------
    dict
        Returns a nested dict. See the public functions for examples
    """
    data = _ESA_special_concerns(in_layer, wc)
    data_list = [util.list_to_dict(y) for y in data]
    log.debug("Where clause = {}, data_list = {}".format(wc, data_list))
    endangered = {}
    for row in data_list:
        try:
            if any(val in status_list for key, val in row.items()):
                try:
                    endangered[(row['Species'])] = row
                except KeyError:
                    try:
                        endangered[(row['Common_name'])] = row
                    except KeyError:
                        log.warning('KeyError in find endangered with wc = {}'.format(wc))
                        # TODO - A reason this could fail is because Thomas updated temporarily
                        # Later on in "prod" this shouldn't fail hopefully
                        pass
        except AttributeError:
            pass
    if len(endangered) == 0:
        endangered['NA'] = {'FMA_NM': 'NA', 'State_status': 'NA', 'Federal_status': 'NA'}
    return {return_type: endangered}


def find_endangered_plants(in_layer):
    """
    Get all federally or state ESA-listed plant species found in the unit.

    This function searches for entries in Special Concerns that have their
    statuses in the DESCRIPTION field equivalent to one of: (E)ndangered,
    (T)hreatened, (L)isted (E)ndangered [federal status], or (L)isted
    (T)hreatened [federal status].

    **Entry:** B-4-c

    Parameters
    ----------
    in_fc: str
        Input feature class

    Returns
    -------
    dict
        Returns a nested dictionary of format:

        .. code-block:: python

            {'Plants': {<plant1>: <status>,
                        <plant2>: <status>,
                        etc)


    """
    wc = "ISSUE_NM = 'Threatened and Endangered Species Element Occurrences'"
    statuses = {'E': 'Endangered',
                'T': 'Threatened',
                'LE': 'Endangered',
                'LT': 'Threatened'}
    result = _find_endangered(in_layer, wc, "Plants", statuses.keys())

    # Need to recode the short codes into something more meaningful
    for key, val in result['Plants'].items():
        for status_type in ('Federal_status', 'State_status'):
            if val[status_type] in statuses:
                val[status_type] = statuses[val[status_type]]
        # And fix the under case
        try:
            val['Common_name'] = val['Common_name'].title()
        except KeyError:
            log.warning('KeyError in find endangered with wc = {}'.format(wc))
            pass
    return result


def find_endangered_animals(in_layer):
    """
    Get all federally or state ESA-listed animal species found in the unit.

    This function searches for entries in Special Concerns that have their
    statuses in the DESCRIPTION field equivalent to either 'Endangered' or
    'Threatened'.

    **Entry:** B-5-b

    Parameters
    ----------
    in_fc: str
        Input feature class

    Returns
    -------
    dict
        Returns a nested dictionary of format:

        .. code-block:: python

            {'Species': {<species1>: <status>,
                         <species2>: <status>,
                         etc}


    """
    wc = "ISSUE_NM = 'Species Occurrence - Threatened and Endangered'"
    statuses = ['Endangered', 'Threatened']
    return _find_endangered(in_layer, wc, "Animals", statuses)