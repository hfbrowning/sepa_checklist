# Some utility functions in no particular order
# Note: None of these functions use arcpy - to facilitate testing without mocks

import logging
import os
import smtplib
import uuid
import data
from collections import OrderedDict
from email import Encoders
from itertools import groupby
from operator import itemgetter

import docxtpl
from email.MIMEBase import MIMEBase
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
from email.Utils import COMMASPACE, formatdate

# Optional: add logging
# At this point there is no logging inside utility functions because I
# can't image what I'd want to log here - but the call to get logger is
# included in case I change my mind
log = logging.getLogger(__name__)


class SlicableOrderedDict(OrderedDict):
    """Create an ordered dictionary with option to slice like a list.

    Example
    --------
    .. code-block:: python

        pets = {3: 'turtles', 15: 'goldfish', 1: 'parrot', 5: 'mice'}
        ordered_pets = SlicableOrderedDict(
            sorted({key: val for key, val in pets.items()}.items(), reverse=True)
        )
        # Give me the 2 top-selling pets!
        ordered_pets[:2]

    See Also
    --------
    Source: https://stackoverflow.com/questions/30975339

    """

    def __getitem__(self, k):
        if not isinstance(k, slice):
            return OrderedDict.__getitem__(self, k)
        x = SlicableOrderedDict()
        for idx, key in enumerate(self.keys()):
            if k.start <= idx < k.stop:
                x[key] = self[key]
        return x


class TimberSaleTechniqueError(Exception):
    """
    Custom class to pass a more meaningful error type
    about unknown corporate timber sale techniques.

    See ``check_corporate_techniques``.
    """
    pass


def acres_to_sq_miles(in_acres):
    """Convert acres to square miles.

    Parameters
    ----------
    in_acres: int
        Acres

    Returns
    -------
    float

    """
    return in_acres*0.0015625


def better_strftime(in_item, formatting="%m/%d/%Y"):
    """
    Improve strftime by guarding against non-datetime in_items.

    Parameters
    ----------
    in_item: any type
        Input to attempt time-formatting on
    formatting: str
        Datetime formatting style

    Returns
    -------
    Same type as ``in_item``
        Formatted (or not) in_item

    """
    try:
        return in_item.strftime(formatting)
    except AttributeError:
        return in_item


def calc_soil_acres(in_list, grouplist=('Survey_Number', 'Texture'), focus='Acres'):
    """
    Add together the soil acreage for similar soil surveys.

    This function takes in the soil Survey Number (named 'Survey_Number', usually) and
    the soil texture (default = 'Texture') and returns a condensed list with the acreage
    added together. This reduces confusing duplicates in the soils table.

    Example
    -------
    .. code-block:: python

        in_list = [{'Survey_Number': 301, 'Texture': 'COBBLE', 'Acres': 83.1},
                   {'Survey_Number': 301, 'Texture': 'COBBLE', 'Acres': 13.2},
                   {'Survey_Number': 121, 'Texture': 'LOAM', 'Acres': 61.3}]
        calc_soil_acres(in_list) # with defaults

        # result [{'Survey_Number': 121, 'Texture': 'LOAM', 'Acres': 61.3},
        #         {'Survey_Number': 301, 'Texture': 'COBBLE', 'Acres': 96.3}]


    Parameters
    ----------
    in_list: list of dictionaries
        Soil records from Special Concerns
    grouplist: tuple
        The two fields names in the ``in_list`` that refer to survey number and soil texture.
    focus: str
        The field name in the ``in_list`` that refers to acres.

    Returns
    -------
    list
        Condensed list of dictionaries
    """
    in_list = multikeysort(in_list, list(grouplist))
    new_list = []
    for key, group in groupby(in_list, lambda x: (x[grouplist[0]],
                                                  x[grouplist[1]])):
        acre_list = []
        for x in group:
            # In case any of the "acres" entries are actually not numbers
            try:
                acre_list.append(float(x[focus]))
            except ValueError:
                pass
        acre_total = sum([y for y in acre_list])
        if acre_total != 0:
            new_list.append({grouplist[0]: key[0],
                             grouplist[1]: key[1],
                             focus: acre_total
                             })
    return new_list


def check_corporate_techniques(technique_set):
    """
    Confirm that the corporate TS techniques haven't changed.

    In order to get the even and uneven aged DNR stand acres,
    it is necessary to use a set of technique codes that describe
    each (determined by in-house staff). There are however no
    guarantees that more codes will not at some point be added to
    the timber sale data - and we want to raise an error (flag)
    if an unknown one shows up so we can update the set of known
    techniques.

    Technically, this function will check **any** arbitrary set against
    the known sets and tell you if anything is new or not. Practically
    speaking this is only useful for comparing against the set
    given by the TECHNIQUE_CD field of the corporate ROPA dataset,
    SHARED_LRM.TS_FMA_SV.

    Parameters
    ----------
    technique_set: set
        Set of timber sale technique codes

    Returns
    -------
    None

    Raises
    ------
    TimberSaleTechniqueError
        Unknown (probably new) TECHNIQUE_CD

    """

    known_techniques = set(data.UNEVEN_CODES).union(set(data.EVEN_CODES))
    difference = list(technique_set - known_techniques)
    if len(difference) != 0:
        msg = "Unknown technique(s): '{}'".format(join_args(difference))
        raise TimberSaleTechniqueError(msg)


def create_temp_file(gdb_path):
    """Create a temp file name in the given path.

    Parameters
    ----------
    gdb_path: str
        path to geodatabase where feature class will be written.

    Returns
    -------
    str
        String corresponding to full path of gdb joined to a junk fc name.
    """
    junk_name = 't{}'.format(uuid.uuid4().hex[:10])
    return os.path.join(gdb_path, junk_name)


def docx_render(template_path, context, out_docx):
    """Pass a dictionary into docxtpl to create a Word doc.

    Parameters
    ----------
    template_path: str
        Full path to template .docx on disk
    context: dict
        Dictionary for filling out template with content
    out_docx: str
        Full path to output .docx

    """
    doc = docxtpl.DocxTemplate(template_path)
    doc.render(context)
    doc.save(out_docx)


def format_soils(in_data_list, survey_str='Survey_Number', acre_str='Acres', texture_str='Texture'):
    """
    Apply all needed formatting to Special Concerns soils data for the checklist table.

    This function is a wrapper around ``list_to_dict``, ``calc_soil_acres``, and
    ``group_soil_textures``. The optional string parameters are to guard against
    possible changes in the Special Concerns Description field.

    Parameters
    ----------
    in_data_list: list
        List of lists from Special Concerns
    survey_str: str (optional)
        Name of Survey Number string.
    acre_str: str (optional)
        Name of Acres string.
    texture_str: str (optional)
        Name of Texture string.

    Returns
    -------
    list
        Formatted list of dictionaries
    """
    unique_data = [list(x) for x in set(tuple(x) for x in in_data_list)]
    list_of_dicts = [list_to_dict(y) for y in unique_data if list_to_dict(y)[texture_str] != 'None']

    # Furthermore make sure that it's unique at least based upon Survey Number, Texture, and Acres
    completely_unique = kludgy_deduper(list_of_dicts)

    calced = calc_soil_acres(completely_unique,
                             grouplist=(survey_str, texture_str), focus=acre_str)
    return group_soil_textures(calced, round_int=2,
                               grouplist=(survey_str, acre_str), focus=texture_str)


def ft_to_miles(linear_feet):
    """Convert linear feet to miles.

    Parameters
    ----------
    linear_feet: int
        Linear feet

    Returns
    -------
    float
    """
    return linear_feet*0.00018939


def group_soil_textures(in_list, round_int=2, grouplist=('Survey_Number', 'Acres'), focus='Texture'):
    """
    Concatenate soil textures for similar soil surveys.

    This function takes in the soil Survey Number (named 'Survey_Number', usually) and
    the soil acreage (default = 'Acres') and returns a condensed list with the textures
    string concatenated. This is necessary to do because the DNR soils data has
    identical, overlapping polygons for soil complexes, which otherwise leads to
    double-counted acres.

    Example
    -------
    .. code-block:: python

        in_list = [{'Survey_Number': 301, 'Texture': 'COBBLE', 'Acres': 83.1},
                   {'Survey_Number': 301, 'Texture': 'STONY GRAVEL', 'Acres': 83.1},
                   {'Survey_Number': 121, 'Texture': 'LOAM', 'Acres': 61.3}]
        group_soil_textures(in_list) # with defaults

        # result [{'Survey_Number': 121, 'Texture': 'LOAM', 'Acres': 61.3},
        #         {'Survey_Number': 301, 'Texture': 'COBBLE/STONY GRAVEL', 'Acres': 83.1}]


    Parameters
    ----------
    in_list: list of dictionaries
        Soil records from Special Concerns
    grouplist: tuple
        The two fields names in the ``in_list`` that refer to survey number and soil acres.
    focus: str
        The field name in the ``in_list`` that refers to texture.

    Returns
    -------
    list
        Condensed list of dictionaries

    """
    in_list = multikeysort(in_list, list(grouplist))
    for x in in_list:
        x['Acres'] = round(x['Acres'], round_int) # Rounding allows better acre grouping
    new_list = []
    for key, group in groupby(in_list, lambda x: (x[grouplist[0]],
                                                  x[grouplist[1]])):
        new_list.append({grouplist[0]: key[0],
                         grouplist[1]: key[1],
                         focus: "/".join(str(x[focus]) for x in group)
                         })
    return new_list


def join_args(args):
    """
    Join the entries of an iterable into a string.

    Example
    -------
    .. code-block:: python

    cat_names = ["Cleo", "Fluffy", "Jinxy"]
    join_args(cat_names)
    # result "Cleo', 'Fluffy', 'Jinxy"


    Parameters
    ----------
    args: iterable
        Elements to be joined

    Returns
    -------
    str
    """
    return "', '".join([str(x) for x in args])


def kludgy_deduper(in_list):
    """
    This is a quick fix function for de-duping soil entries.

    This function was made because of the observation that if you
    have two soil entries with identical Survey Numbers, Textures and
    Acres (but non-identical other entries) that these were slipping
    through my other de-duplicator. This function however uses
    hard-coded entries for all of those fields so could stand to be
    fixed...

    Parameters
    ----------
    in_list: list
        List of dictionaries

    Returns
    -------
    list
        De-duplicated list of dictionaries

    """
    key_dict = {}
    for index, entry in enumerate(in_list):
        # This naturally removes duplicates on the list index
        key_dict[(entry['Texture'], entry['Survey_Number'], round(float(entry['Acres']), 3))] = index

    return [in_list[i] for i in key_dict.values()]


def list_to_dict(in_list):
    """Take a list from Special Concerns and convert to dict.

    Special Concerns' description fields tend to be formatted as a
    single string with pipes separating content. An example::

        'Habitat type: Prairie | Source: WDFW | Management type: No Manage'


    This format is already parsed by _special_concerns() into a list::

        ['Habitat type: Prairie', 'Source: WDFW', 'Management type: No Manage']


    This function converts it further into::

        {'Habitat_type': 'Prairie', 'Source': 'WDFW', 'Management_type': 'No Manage'}

    Parameters
    ----------
    in_list: list
        List output from _special_concerns()

    Returns
    -------
    dict
    """
    new_dict = {}
    for x in in_list:
        x_split = x.split(': ', 1)
        if len(x_split) > 1:
            # Keys cannot have a space to work with Jinja/Docxtpl
            key = x_split[0].replace(" ", "_")
            val = x_split[1]
            new_dict[key] = val
        else:
            continue
    return new_dict


def multikeysort(items, key_list):
    """
    Helper function to allow sorting on multiple input fields.

    See usage in ``calc_soil_acres`` or ``group_soil_textures``.

    See Also
    --------
    Source: https://stackoverflow.com/questions/1143671

    Parameters
    ----------
    items: list
        List to sort
    key_list: list
        List of keys to sort on in descending order of importance

    Returns
    -------
    list
        Sorted list
    """
    comparers = [((itemgetter(col[1:].strip()), -1) if col.startswith('-') else
                  (itemgetter(col.strip()), 1)) for col in key_list]
    def comparer(left, right):
        for fn, mult in comparers:
            result = cmp(fn(left), fn(right))
            if result:
                return mult * result
        else:
            return 0
    return sorted(items, cmp=comparer)


def remove_bracket(in_str):
    """Remove angled bracket(s) from string.

    Parameters
    ----------
    in_str: str
        Input string to remove brackets from

    Returns
    -------
    str
        New string without brackets. If input not type string, function returns None.

    """
    try:
        return in_str.replace('<', '').replace('>', '')
    except AttributeError:
        pass


def replace_bad_chars(in_ts_name):
    """Make directory-friendly timber sale name.

    Remove non-alphanumeric chars from a timber sale name and prepend
    ``SaleName_`` to name if name begins with a digit. This ensures that
    folders created off the timber sale name are valid.

    Parameters
    ----------
    in_ts_name: str
        Timber sale name

    Returns
    -------
    str
        New string without invalid characters in it

    """
    bad_characters = ["'", "/", "\\", "-", ":", "(", ")", "<", ">", "|", "?", "*", " ", "&", "#", ",", "("''")", " "]
    bad_numbers = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
    new_ts_name = str(in_ts_name)
    for ch in bad_characters:
        if ch in new_ts_name:
            new_ts_name = new_ts_name.replace(ch, "_")
        for num in bad_numbers:
            # get first character/number in the name
            char_1 = new_ts_name[0][0]
            if char_1 == num:
                new_ts_name = "SaleName_" + str(new_ts_name)
    return new_ts_name


def send_mail(mail_server, fro, to, subject, text, files=[]):
    """Send an email.

    Parameters
    ----------
    fro: str
        Sender's email address
    to: list
        List of recipients
    subject: str
        Subject line
    text: str
        Body of email
    files: (str, optional)
        Files to attach to email. Defaults to None.

    Returns
    -------
    None
        Sends email (side effect)
    """
    assert type(to) == list
    assert type(files) == list
    msg = MIMEMultipart()
    msg['From'] = fro
    msg['To'] = COMMASPACE.join(to)
    msg['Date'] = formatdate(localtime=True)
    msg['Subject'] = subject
    msg.attach(MIMEText(text))
    for file in files:
        part = MIMEBase('application', "octet-stream")
        part.set_payload(open(file, "rb").read())
        Encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment; filename="%s"' % os.path.basename(file))
        msg.attach(part)
    # serverName = "mail.dnr.wa.gov"
    # smtp = smtplib.SMTP(serverName)
    smtp = smtplib.SMTP(mail_server)
    smtp.sendmail(fro, to, msg.as_string())
    smtp.close()



