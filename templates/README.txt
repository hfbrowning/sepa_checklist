A readme to describe the contents of 'templates'
Hilary Browning
1:36 PM 9/13/2018

This folder only contains two .docx files, blank_template and template:

- template.docx:: for creating an autofilled SEPA. It contains template language from doctxpl (very similar to jinja2) to help it render the dictionary that comes out of the sepa checklist scripts.
- blank_template.docx:: identical to template.docx except that all templating language/tags and boilerplate language have been removed. It is essentially a blank SEPA Checklist. This is helpful for doing a diff/compare check with changes from the SEPA Center as they come in - otherwise you're comparing against tags that they won't have. 