A readme to describe the contents of 'tests'
Hilary Browning
8:44 AM 9/14/2018

This folder contains a loose collection of files that are intended the test the script and output in various ways:

- Any python scripts (test_util.py, __init__.py) correspond to unit testing of functions for SEPA Checklist. Unit testing scripts are named based upon what module they test; for example, test_util.py provides some testing of util.py. Other unit tests made in the future should follow the same naming convention. 

- Any binary files (static_context.p) are made from the cPickle module for rapid prototyping of new ideas or demonstration. In this case, static_context.p is the saved context dictionary from a particular run of the SEPA Checklist. It has proven useful for explaining to people the structure of the output, without needing to wait for a new run.

- Any csv (runner.csv, runner_notes.csv, sanity_check_data.csv) exist for running a batch process of the checklist (using making_batch_csv.py) off of 7 known timber sales. This is a form of regression testing; if the data in sanity_check_data.csv alter significantly from a known good state, then something has changed either in the underlying data or in scripts elsewhere, that needs to be corrected. Runner.csv can also be altered to run other timber sales in batch mode, although it is recommended that names of files be changed first. 

